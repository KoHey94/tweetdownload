import requests,sys
from bs4 import BeautifulSoup
args = sys.argv
scraping_url = args[1]
dir_path = args[2]
timestamp = args[3]
response = requests.get(scraping_url)
soup = BeautifulSoup(response.text, 'lxml')
links = soup.select('.js-original-tweet img')

pic_index = 0
for link in links:
    if 'https://pbs.twimg.com/media/' in str(link):
        pic_index+=1
        url = link.get('src')
        picname = timestamp + '_' + str(pic_index) + '.jpg' 
        path_pic = dir_path + picname
        req = requests.get(url)
        if response.status_code == 200:
            f = open(path_pic, 'wb')
            f.write(req.content)
            f.close()
            print('download ' + url + ' as ' + picname)
